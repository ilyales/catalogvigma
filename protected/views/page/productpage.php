<div ng-app="productpage">
<div ng-controller="productpageController" ng-init="init()">

<searchblock search-callback="search.startSearch"></searchblock>
<a href="/catalog/cart" class="cartlink">
	<img class="cartlink_img" src="/catalog/img/cart-black.png">
	<div class="cartlink_text">Корзина</div>
</a>

<div class="main-content">
	<div class="categories">
		<div class="categories_all" ng-click="search.searchAll()">Все запчасти для спецтехники ЧТЗ</div>

			<div class="categories_item"
			     ng-repeat="catalogcat in catalogcats" 
			     ng-click="search.selectCatalogcat(catalogcat)"
				 ng-class="{'categories_item__select':catalogcat.id==search.selectedCatalogcatId}"		     
			>
				<div class="categories_decor"></div>
				<div class="categories_name">{{catalogcat.name}}</div>
			</div>
		
	</div>
	<div class="ppage">
		<div class="ppage-top">
			<div class="ppage-top_clm1">
				<?php 
					if ($product['img']!=null) $imgUrl = '/catalog'.$product['img'];
					else $imgUrl = '/catalog/img/nophoto.png';
				?>
				<img class="ppage-top_photo" 
					 src="<?php echo $imgUrl ?>"
				>


			</div>
			<div class="ppage-top_clm2">
				<div class="ppage-top_info-label">Наименование:</div>
				<div class="ppage-top_info-val"><?php echo $product['catalog_name']?></div>

				<div class="ppage-top_info-label">Обозначение:</div>
				<div class="ppage-top_info-val"><?php echo $product['catalog_chod']?></div>

				<?php if ($product['weight']!=null): ?>
					<div class="ppage-top_info-label">Вес:</div>
					<div class="ppage-top_info-val"><?php echo $product['weight']?> кг</div>
				<?php endif; ?>

				<?php if ($product['size']!=null): ?>
					<div class="ppage-top_info-label">Габариты:</div>
					<div class="ppage-top_info-val"><?php echo $product['size']?> см</div>
				<?php endif; ?>
			</div>
			<div class="ppage-top_clm3">
				<!-- 
				<div class="ppage-top_info-label">Вес:</div>
				<div class="ppage-top_info-val">14 кг</div>

				<div class="ppage-top_info-label">Габариты:</div>
				<div class="ppage-top_info-val">12 * 14 * 120 см</div>
				 -->
				<!-- <div class="ppage-top_btn ppage-top_btn__1">Узнать цену Вигмы</div> -->
				
				<div class="product-top_price-wr" ng-cloak>
					<div class="ppage-top_price-label">Цена ЧТЗ - Уралтрак:</div>
					<div class="ppage-top_price-val">{{numberFormat(product.price_catalog)}} руб. С НДС</div>

					<!-- <div class="ppage-top_btn ppage-top_btn__2">Купить</div>
					<div class="ppage-top_btn ppage-top_btn__3">Заказать</div>
					<div class="ppage-top_btn ppage-top_btn__4">Получить скидку</div> -->
					<addtocart class="ppage_addtocart" 
					           countbox-class="ppage-addtocart_count-box" 
					           product-id="product.id" btn-label="Добавить в коризину" 
					           btn-img="/catalog/img/cart.png"
					>
					</addtocart>
					<!--
					<div class="ppage-top_btn ppage-top_btn__buybtn" ng-click="contactForm.show()" ng-show="!cart.added">
						<img class="ppage-top_btn__buybtn-i" src="/catalog/img/one-click.png">
						<span class="ppage-top_btn__buybtn-t">Купить в один клик</span>
					</div>
					-->
				</div>
			</div>
		</div>

		<div class="ppage-discr">
			<div class="ppage-discr_title">Описание:</div>
			<div class="ppage-discr_text">
				<?php echo $product['original_name']?> <?php echo $product['chod_display']?> применяется в составе <span>{{product.catalogcats_str}}</span> <span>{{product.groupsNums_str}}</span> бульдозера, трактора или трубоукладчика.
				<br>Применяется на технике выпущенной на заводе ЧТЗ-Уралтрак в Челябинске
			</div>
		</div>

		<div class="ppage-oinfo">
			<div class="ppage-oinfo_line">
				<div class="ppage-oinfo_label">Производство:</div>
				<div class="ppage-oinfo_val">Россия</div>
			</div>

			<div class="ppage-oinfo_line">
				<div class="ppage-oinfo_label">Гарантия:</div>
				<div class="ppage-oinfo_val">6 месяцев</div>
			</div>

			<div class="ppage-oinfo_line ppage-oinfo_line__stocks">
				<div class="ppage-oinfo_label">Наличие:</div>
				<div class="ppage-oinfo_val">
					<div class="ppage-stocks">
						<div class="ppage-stocks_row"
							 ng-repeat="stock in product.stocks"
						>
							<div class="ppage-stocks_city">{{stock.name}}</div>
							<div class="ppage-stocks_inStock">
								<div class="ppage-stocks_yes" ng-show="stock.status=='inStock'">в наличии</div>
								<div class="ppage-stocks_inChe" ng-show="stock.status=='inChelyabinsk'">2-3 дня</div>
								<div class="ppage-stocks_confirm" ng-show="stock.status=='needClarify'" ng-click="contactForm.show('checkAvailability')" >уточнить наличие</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="ppage-similar">
			<div class="ppage-similar_title">С этой запчастью так же покупают:</div>

			<div  class="ppage-similar_item" >
				<div class="ppage-similar_item-img-wr">
					<img class="ppage-similar_item-img" src="/catalog/img/nophoto.png">				
				</div>
				<div class="ppage-similar_item-info-wr">
					<div class="ppage-similar_item-name">Гусеница серийная 5ти катковая</div>
					<div class="ppage-similar_item-chod">50-22-9-СП</div>
				</div>			
			</div>
			<div  class="ppage-similar_item" >
				<div class="ppage-similar_item-img-wr">
					<img class="ppage-similar_item-img" src="/catalog/img/nophoto.png">				
				</div>
				<div class="ppage-similar_item-info-wr">
					<div class="ppage-similar_item-name">Каток двубортный</div>
					<div class="ppage-similar_item-chod">24-21-170СП</div>
				</div>			
			</div>
		</div> -->

	</div>
</div>


<div class="cat-contact-form" ng-show="contactForm.visible" ng-cloak>
	<div class="cc-form">
		<img class="cc-form_close" 
			 src="/catalog/img/close-big-black.png"
			 ng-click="contactForm.close()"
		>

		<div class="cc-form_head">
			<div class="cc-form_mode">
				<span ng-show="contactForm.mode=='checkAvailability'">Уточнить наличие:</span>
			</div>
			<div class="cc-form_product"><?php echo $product['original_name']?> <br> <?php echo $product['chod_display']?></div>
		</div>
		<div class="cc-form_info">Пожалуйста, оставьте Ваш контактный телефон, мы свяжемся с Вами при первой возможности!</div>
		<div class="cc-form_line">
			<div class="cc-form_label">Имя</div>
			<div class="cc-form_required" ng-show="!contactForm.validValues.name">обязательное поле</div>
			<input type="text" 
				   class="cc-form_val" 
				   ng-model="contactForm.values.name"
				   ng-class="{'cc-form_val__error':!contactForm.validValues.name}"
			>
		</div>
		<div class="cc-form_line">
			<div class="cc-form_label">Телефон</div>
			<div class="cc-form_required" ng-show="!contactForm.validValues.phone">обязательное поле</div>
			<input type="text" 
				   class="cc-form_val" 
				   ng-model="contactForm.values.phone"
				   ng-class="{'cc-form_val__error':!contactForm.validValues.phone}"
			>
		</div>
		<div class="cc-form_line">
			<div class="cc-form_label">Сообщение</div>
			<textarea class="cc-form_text" ng-model="contactForm.values.text"></textarea>
		</div>
		<div class="cc-form_sendBtn" ng-click="contactForm.send()">Отправить</div>

	</div>	
	<div class="cc-form-suc" ng-show="contactForm.sendSuccess" ng-cloak>
		<div class="cc-from-suc_text">Отправлено!</div>
		<div class="cc-form-suc_closeBtn" ng-click="contactForm.close()">Закрыть</div>
	</div>
</div>
<div class="cc-form-bg" ng-show="contactForm.visible" ng-click="contactForm.close()" ng-cloak></div> 


</div>
</div>

<script type="text/javascript">
	
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var product = <?php echo CJavaScript::encode($product)?>;
	console.log(product);
</script>