<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';

?>

<div class="errorPage">
	<h2 class="errorPage_title">Ошибка <?php echo $code; ?></h2>

	<div class="errorPage_message">
		<?php echo CHtml::encode($message); ?>
	</div>	

</div>

