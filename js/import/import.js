angular.module('Importpage', ['ngFileUpload']);

angular.module('Importpage').factory('Waitbox', function() {
 	var scope;
 	return {    	
    	
  		show:function(t)
  		{
  			scope.text = t;
  			scope.visible = true;  			
  		},

  		close:function() 
  		{
  			scope.text = null;
  			scope.visible = false;
  		},
 
  		text:function(t)
  		{
  			scope.text=t;
  		},

  		done:function()
  		{
  			scope.loadingVisible = false;
  			scope.closeVisible = true;
  		},

  		setScope:function(s)
  		{
  			scope = s;
  		},
  	}
  
});

angular.module('Importpage').directive('waitbox', ['Waitbox', function (Waitbox) {
    return {
    	template: 	'<div class="waitBox" ng-show="visible">'+
					'	<div class="waitBox_text" ng-bind="text"></div>' + 
					'	<img class="waitBox_img" ng-show="loadingVisible" src="/catalog/img/loading.gif">' +
					'   <button class="btn btn-success waitBox_close" ng-show="closeVisible" ng-click="close()">Закрыть</button>' +
					'</div>',

		link: function (scope, element, attrs) {
        	scope.visible = false;
        	scope.loadingVisible = true;
        	scope.closeVisible = false;
            scope.text = "";
            
            Waitbox.setScope(scope);
            
            scope.close = function() 
            {
            	Waitbox.close();
            }
        }
    };
}]);

angular.module('Importpage').controller('ImportController',function($scope,$http,Upload,Waitbox) {

	$scope.importDb	= {

		result:null,
		importing:false,

		file: {
			name:null,//"файл не выбран",
			loading:false,
			loaded:false,

			upload:function(file)
			{	
				

				$scope.importDb.file.loading =  true;
				$scope.importDb.file.loaded = false;
				Upload.upload({
			        url: '/catalog/fileApi/UploadFileAjax',
			        data: {file: file,
 				           uploadType:'zip',
			        	  },
			        
			    }).then(function (resp) {
			    	$scope.importDb.file.loading = false;
			   		$scope.importDb.file.loaded = true;
			        $scope.importDb.file.name = resp.config.data.file.name;
			   		
			    }, function (resp) {
			        $scope.file.name = "Ошибка загрузки: ".resp.status;
			        $scope.file.loading = false;
			    }, function (evt) {
			        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			        // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
			     });
			},
		},

		startImport:function() {
			Waitbox.show('1/3 Разархивирование...');
			
			var url = '/catalog/importApi/unzipAjax';
			$http.post(url,{
				fileName: $scope.importDb.file.name,
			}).success(function (data) {
				if (data.status)
				{
					Waitbox.text("2/3 Ипорт дампа базы данных..");

					url = '/catalog/importApi/importDbAjax';
					$http.post(url,{
						
					}).success(function (data) {
						if (data.status)
						{
							Waitbox.text("3/3 Копировние фотографий");
							url = '/catalog/importApi/copyPhotosAjax';
							$http.post(url,{
								
							}).success(function (data) {
								if (data.status)
								{
									Waitbox.text("Иморт каталога завершен!");
									Waitbox.done();
								}
								else
								{
									Waitbox.text("Ошибка: " + data.message);
									Waitbox.done();
								}
							});
						}
						else
						{
							Waitbox.text("Ошибка: " + data.message);
							Waitbox.done();
						}
					});
				}
				else
				{
					Waitbox.text("Ошибка: " + data.message);
				}
				
			});

			/*
			var url = '/catalog/index.php?r=importApi/importDbAjax';
			$http.post(url,{
				fileName: $scope.importDb.file.name,
			}).success(function (data) {
				if (data.status)
				{
					$scope.importDb.result = "Импорт завершен!";
				}
				else
				{
					$scope.importDb.result = "Во время импорта произошла ошибка:"+data.message;
				}
				Waitbox.close();
			});
			*/
		},

		translit:function() {
			Waitbox.show('Транслитерация...');
			var url = '/catalog/importApi/translitAjax';
			$http.post(url,{
				
			}).success(function (data) {
				if (data.status)
				{
					alert("Транслит завершен!");
				}
				else
				{
					alert("Во время транслита произошла ошибка:"+data.message);
				}
				Waitbox.close();
			});
		}
	}
	
   
});