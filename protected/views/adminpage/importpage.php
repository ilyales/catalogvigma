<div ng-app="Importpage" class="importpage">
<div ng-controller="ImportController" ng-init="init()">

<div class="import-title">Импорт базы данных</div>

<div class="import-ctrl">
	<div class="import-ctrl_filename">{{importDb.file.name}}</div>
	<button class="btn import-ctrl_fileLoadBtn"  ngf-select="importDb.file.upload($file)">Выбрать файл</button>
	<img class="import-ctrl_loading" src="/catalog/img/loading.gif" ng-show="importDb.file.loading">
	<button class="btn btn-success import-ctrl_startImportBtn" 
			ng-show="importDb.file.loaded"
			ng-click="importDb.startImport()"
	>Начать импорт</button>
	<img class="import-ctrl_importing" src="/catalog/img/loading.gif" ng-show="importDb.importing">
</div>

<div class="import-results">
	{{importDb.result}}
</div>



<waitBox></waitBox>	
</div>	
</div>