<div ng-app="catalog">
<div ng-controller="catalogController" ng-init="init()" ng-cloak>

<searchblock search-callback="search.startSearch"></searchblock>
<a href="/catalog/cart" class="cartlink">
	<img class="cartlink_img" src="/catalog/img/cart-black.png">
	<div class="cartlink_text">Корзина</div>
</a>

<div class="main-content">
	<div class="categories">
		<div class="categories_all" ng-click="search.searchAll()">Все запчасти для спецтехники ЧТЗ</div>

			<div class="categories_item"
			     ng-repeat="catalogcat in catalogcats" 
			     ng-click="search.selectCatalogcat(catalogcat)"
				 ng-class="{'categories_item__select':catalogcat.id==search.selectedCatalogcatId}"		     
			>
				<div class="categories_decor"></div>
				<div class="categories_name">{{catalogcat.name}}</div>
			</div>
		
	</div>
	<div class="results" ng-cloak>
		<div class="result_list"
			 tasty-table
			 bind-init="search.ngTastyInit"
			 bind-resource-callback="search.getResource" 
			 bind-filters="search.filter"
		>
			<div class="result_list-in">	
			<div class="result_item" ng-repeat="product in rows">
				<a ng-href="/catalog/product/{{product.url}}" class="result_item-img-wr">
					<img class="result_item-img" 
						 ng-src="{{(product.img!=null) ? '/catalog'+product.img : '/catalog/img/nophoto.png'}}"
					>				
				</a>

				<div class="result_item-info-wr">
					<a ng-href="/catalog/product/{{product.url}}" class="result_item-names-wr">
						<div class="result_item-name">{{product.catalog_name}}</div>
						<div class="result_item-chod">{{product.catalog_chod}}</div>
					</a>

					<div class="result_item-btns">
						<addtocart class="result_addtocart" countbox-class="result-addtocart_count-box" product-id="product.id" btn-label="В коризину"></addtocart>
						
					</div>
				</div>
			</div>
			</div>

			<div class="pag" tasty-pagination bind-items-per-page="itemsPerPage" bind-list-items-per-page="listItemsPerPage"></div>
		</div>
	</div>
</div>

</div>
</div>

<script type="text/javascript">
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var initSearchStr = '<?php echo $initSearchStr?>';
	var catalogcat = '<?php echo $catalogcat?>';
</script>