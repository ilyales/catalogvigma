<?php

class UploadFile {

	public function attributeLabels()
	{
		return array();
	}

	public static function getUploadDirPath($uploadType)
	{
		$ds=DIRECTORY_SEPARATOR;
        
        switch ($uploadType) {
            case 'db':
                $path = Yii::app()->basePath.$ds."..".$ds."dumps".$ds;
                break;
            case 'zip':
                 $path = Yii::app()->basePath.$ds."..".$ds."uploadzip".$ds;
                 break;
        }

		return $path;
	}

    public static function upload($file,$uploadType)
    {
    	
        $newFilePath = self::getUploadDirPath($uploadType).$file["name"];
        //если запущен на виндоус - применить кодировку к файлу
        if (DIRECTORY_SEPARATOR == '\\')        $newFilePath = iconv('utf-8','windows-1251',$newFilePath);
        $res =move_uploaded_file($file["tmp_name"], $newFilePath);

        return $res;
    }

    public static function getPathByName($fileName,$uploadType)
    {
    	$filePath = self::getUploadDirPath($uploadType).$fileName;
        if (DIRECTORY_SEPARATOR == '\\') $filePath = iconv('utf-8','windows-1251',$filePath);
        return $filePath;
    }

}