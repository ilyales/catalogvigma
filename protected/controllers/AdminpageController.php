<?php

class AdminpageController extends CController
{
	
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

    

	public $layout='/layouts/catalogLayout';

	public function actionImport() 
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerPackage('importPage');
		$this->render('importpage');
	}

	public function actionTest()
	{
		Import::copyPhotos();
	}
}
