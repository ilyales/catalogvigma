angular.module('addtocart',['countbox']);

angular.module('addtocart').directive('addtocart',['$http','$q','$compile','$timeout', function($http,$q,$compile,$timeout) {        
	return {
		template: '<div id="addtocart_addBtn" class="addtocart_addBtn" ng-click="cart.addToCart()" ng-cloak><img ng-show="btnImg!=null" class="addtocart_addBtn-img" src="{{btnImg}}"> <span class="addtocart_addBtn-text">{{btnLabel}}</span></div>',

		replace: false,
		scope:{
			productId:'=',
			countboxClass:'@',
			btnLabel:'@',
			btnImg:'@',
			
		},
		restrict: 'E',


		link: {
			pre:function(scope, element, attrs) {
				
			},

			post:function (scope, element, attrs) {
				var cart = {
					addToCart:function(product_id) {

						var self = this;

						var addBtnEl = angular.element(element[0].querySelector('#addtocart_addBtn'));
						addBtnEl.addClass('addtocart_addBtn__fadeout');

						var url = '/catalog/cartApi/add';
						$http.post(url,{
							product_id:scope.productId,
							count:1,
						}).success(function (data) {
							if (data.status)
							{	

								self.showCountbox();	
							}
							else
							{
								
							}
						});
					},

					showCountbox:function()
					{
						
						var addBtnEl = angular.element(element[0].querySelector('#addtocart_addBtn'));
						
						addBtnEl.addClass('addtocart_addBtn__hide');

						var template =  '<div class="addtocart_count-wr">' + 
											'<a href="/catalog/cart" class="addtocart_count-label">В корзине:</a>' +
											'<countbox class="' + scope.countboxClass +'" countbox-value="cart.productCount" countbox-change="cart.onChangeCartProductCount()"></countbox>' +
										'</div>';
			            var linkFn = $compile(template);
			            var countWrEl = linkFn(scope);

			            element.append(countWrEl);

						setTimeout(function(){
							countWrEl.addClass('addtocart_count-wr__visible');

						},50);
						
					},

					changeCartProductCart:function()
					{
						$timeout(function(){				

							var url = '/catalog/cartApi/changeCount';
							$http.post(url,{
								product_id:scope.productId,
								count:scope.cart.productCount,
							}).success(function (data) {
								if (data.status)
								{
																											
								}
								else
								{
									
								}
							});
						});
					},

					init:function(){
						var self = this;
						scope.cart = {
							productCount:1,
							addToCart:self.addToCart.bind(self),
							onChangeCartProductCount:self.changeCartProductCart.bind(self),
						}

						// if (scope.btnImg!=null)
						// {
						// 	element.html('<div id="addtocart_addBtn" class="addtocart_addBtn" ng-click="cart.addToCart()"></div>');
						// }
												
					},
				}

				cart.init();

      		},

		},
			
	}
}]);