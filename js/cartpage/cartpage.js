var CartPageApp=angular.module('CartPageApp', ['countbox']);

CartPageApp.controller('CartController', function($scope,$http,$timeout) {

	$scope.init = function() {
		cart.init();
		reqForm.init();

	}

	var cart = {

		init:function() 
		{
			var self = this;
			$scope.cart = {
				cartProducts:cartProducts,
				onChangeCartProductCount:self.changeCartProductPrice.bind(self),
				deleteCartProduct:self.deleteCartProduct.bind(self),
				onClearCart:self.clearCart.bind(self),
			}
			
			this.calcTotalPrice();
		},

		changeCartProductPrice:function(cartProduct)
		{
			var self = this;
			
			//cartProduct.count - примет новое значение по окончанию digest цикла,
			//поэтому отправку делаем в обертке $timeout для запуска в следуюем digest цикле			
			$timeout(function(){				

				var url = '/catalog/cartApi/changeCount';
				$http.post(url,{
					product_id:cartProduct.product.id,
					count:cartProduct.count,
				}).success(function (data) {
					if (data.status)
					{
						cartProduct.price = cartProduct.count*cartProduct.product.price_catalog;
						self.calcTotalPrice();
						
					}
					else
					{
						
					}
				});
			});			
		},

		calcTotalPrice:function(){
			var totalPrice = 0;
			for (var i=0;i<$scope.cart.cartProducts.length;i++)
			{
				totalPrice = totalPrice + $scope.cart.cartProducts[i].price;
			}

			$scope.cart.totalPrice = totalPrice;
		},


		deleteCartProduct:function(delete_index)
		{
			var self=this;
			var product_id = $scope.cart.cartProducts[delete_index].product.id;

			var url = '/catalog/cartApi/delete';
			$http.post(url,{
				product_id:product_id,
			}).success(function (data) {
				if (data.status)
				{
					$scope.cart.cartProducts.splice(delete_index,1);
					self.calcTotalPrice();
				}
				else
				{
					
				}
			});
		},

		clearCart:function()
		{
			var self = this;
			
			var url = '/catalog/cartApi/clearcart';
			$http.post(url,{
				
			}).success(function (data) {
				if (data.status)
				{
					$scope.cart.cartProducts = [];
					self.calcTotalPrice();
				}
				else
				{
					
				}
			});
		},		
	}

	var reqForm = {

		init:function()
		{
			this.bindScroll();

			var self = this;
			$scope.reqForm = {
				onSendClick:self.onSendClick.bind(self),

				sendProcessing:false,
				emptyCartWarning:false,
				sendSuccess:false,

				values:{
					name:null,
					phone:null,
					text:null,
				},

				validValues:{
					name:true,
					phone:true,
				},


			}
						
		},		
		

		onSendClick:function(){


			if ($scope.cart.cartProducts==null || $scope.cart.cartProducts.length==0)
			{
				console.log('emptyCartWarning');
				$scope.reqForm.emptyCartWarning = true;

				$timeout(function(){
					$scope.reqForm.emptyCartWarning = false;
				},2000);
				return;
			}

			var valid = true;
			$scope.reqForm.validValues.name = true;
			$scope.reqForm.validValues.phone = true;

	    	if ($scope.reqForm.values.name==null || $scope.reqForm.values.name=="")
	    	{
	    		$scope.reqForm.validValues.name = false;
	    		valid = false;
	    	}

	    	if ($scope.reqForm.values.phone==null || $scope.reqForm.values.phone=="")
	    	{
	    		$scope.reqForm.validValues.phone = false;
	    		valid = false;
	    	}

	    	if (valid)
	    	{
	    		this.showSendProcessing();
	    		this.sendForm().success((function (data) {
	    			if (data.status)
	    			{
	    				this.hideSendProcessing();
	    				this.showSendSuccess();
	    			}
	    		}).bind(this));
	    	}
		},

		sendForm:function() {
			var url = "/catalog/formApi/cartRequisitionAjax";

			return $http.post(url,{
				name:$scope.reqForm.values.name,
				phone:$scope.reqForm.values.phone,
				text:$scope.reqForm.values.text,				
			});
		},

		showSendProcessing:function() 
		{
			$scope.reqForm.sendProcessing = true;
		},

		hideSendProcessing:function() 
		{
			$scope.reqForm.sendProcessing = false;
		},

		showSendSuccess:function() 
		{
			var cartEditorEl = angular.element(document.getElementById('cart-editor'));
			cartEditorEl.addClass('cart-editor__hide');
			setTimeout(function(){
				cartEditorEl.addClass('cart-editor__hide-full');
				var cartSuccessEl =  angular.element(document.getElementById('cart-success'));
				cartSuccessEl.addClass('cart-success__visible');
			},300);
		},

		bindScroll:function()
		{
			var reqFormEl = angular.element(document.getElementById('reqForm'));

			var ua = navigator.userAgent.toLowerCase();
			var isOpera = (ua.indexOf('opera')  > -1);
			var isIE = (!isOpera && ua.indexOf('msie') > -1);
			 
			var getDocumentHeight = function() {
			  return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, getViewportHeight());
			}

			var getViewportHeight = function() {
			  return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
			}

			angular.element(window).bind("scroll", function()
			{
				
				if ($scope.cart.cartProducts.length<6)
				{
					if (reqFormEl.hasClass('cart-requisition__fixed-bottom')) reqFormEl.removeClass('cart-requisition__fixed-bottom')
           			if (reqFormEl.hasClass('cart-requisition__fixed-top')) reqFormEl.removeClass('cart-requisition__fixed-top');
           			reqFormEl.addClass('cart-requisition__init');
				}

           		else if (this.pageYOffset >= 370) 
           		{
           			var documentHeight = getDocumentHeight();

           			if (reqFormEl.hasClass('cart-requisition__init')) reqFormEl.removeClass('cart-requisition__init');
           			
           			if (documentHeight - this.pageYOffset > 966 )
           			{
           				if (reqFormEl.hasClass('cart-requisition__fixed-bottom')) reqFormEl.removeClass('cart-requisition__fixed-bottom');

						reqFormEl.addClass('cart-requisition__fixed-top');
           			}
           			else
           			{
           				if (reqFormEl.hasClass('cart-requisition__fixed-top')) reqFormEl.removeClass('cart-requisition__fixed-top');
           				reqFormEl.addClass('cart-requisition__fixed-bottom');
           				var topOffset = documentHeight - 966;
           				reqFormEl.css('top',topOffset+'px');
           			}
           			
           		}
           		else
           		{
           			if (reqFormEl.hasClass('cart-requisition__fixed-bottom')) reqFormEl.removeClass('cart-requisition__fixed-bottom')
           			if (reqFormEl.hasClass('cart-requisition__fixed-top')) reqFormEl.removeClass('cart-requisition__fixed-top');
           			reqFormEl.addClass('cart-requisition__init');
           		}

           	});
		},		
	}

});